/* ATTINY85 HUMIDITY CONTROLLER
By: Tyler McLaughlin

Will turn a relay on or off depending on humidity.

TODO:
  //Not using battery anymore... but this was the plan: Blink a 1mA LED attached to zener diode to show voltage is adequate
  Figure out transistor saturation resistor
  Check how well turning the sensor on using the arduino will work
  Fine a 110v relay with 5v coil
  Choose power source... Going with 5v cell phone charger integrated into the box.
  
*/
//SENSOR WIRING
// Connect pin 1 (on the left) of the sensor to +5V
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

//ATTINY85 WIRING
// Connect D2 to sensor data pin
// Connect D1 to relay coil transistor
// Connect xx to sensor VIN
// Connect D3 to serial RX line
// Connect D4 to 1mA LED

#include "DHT.h"
#include <Timer.h>
#include <Event.h>

#define DHTPIN 2     // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)
#define DHTTIME 2.0   // timing param for clock speed DEFAULT: 6, ATTINY85 @ 8mhz: 2.0
#define HIGHRH 75  //When to turn on the dehumidifier initially
#define LOWRH 65  //When to turn the dehumidifier back off

DHT dht(DHTPIN, DHTTYPE, DHTTIME);
Timer t;

int relay = 1; //The relay pin
int threshold = false; //Have we had to turn on the dehumidifier
float humidity; //The var for humidity readings
float temp; //The var for temperature readings

void setup() {
  Serial.begin(9600); 
  Serial.println("Dehumidifier Controller Debug");
  
  pinMode(relay, OUTPUT); //We want the transistor pin to be an output
 
  dht.begin(); //Starts the dht library
  int readData = t.every(2000, dhtRead, (void*)0);
}

void loop() {
  t.update();
  relayControl();
}

void dhtRead(void *context) { //Reads the sensor data
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  humidity = dht.readHumidity();
  temp = dht.readTemperature();

  // check if returns are valid, if they are NaN (not a number) then something went wrong!
  if (isnan(temp) || isnan(humidity)) {
    Serial.println("Failed to read from DHT");
  } else {
    Serial.print("Humidity: "); 
    Serial.print(humidity);
    Serial.print(" %\t");
    Serial.print("Temperature: "); 
    Serial.print(temp*9/5+32); //Convert celcius to F
    Serial.println(" *F");
  }
}

void relayControl() { 
  int state;
  //Turn on the LED if the humidity goes too high
  if (humidity >= HIGHRH) {
    threshold = true;
    digitalWrite(relay, HIGH);
  } 
  if (threshold && humidity >= LOWRH){
    //KEEP RELAY HIGH
    state = digitalRead(1); //Check to make sure it's on
    if (state != true) {
      digitalWrite(relay, HIGH);
    }
  }
  else {
    digitalWrite(relay,LOW);
    threshold = false; 
  }
}
