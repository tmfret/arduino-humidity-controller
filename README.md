* Arduino (Attiny85) Humidity Controller Using DHT Temp/Humidity Sensor

Created by:
    Tyler McLaughlin
    tmfret@gmail.com

This sketch is built on the Attiny85, so you will need the proper board files for your Arduino version. The files I used when I made this may no longer be usable, so I have not included them.

You will need a DHT library. I no longer have the library I used, as this project was started a number of years ago.